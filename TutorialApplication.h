/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include <OgreRoot.h>
#include <OgreWindowEventUtilities.h>
 
#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <OgreEntity.h>
#include <OgreCamera.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <OgreException.h>
 
class TutorialApplication
	: public Ogre::WindowEventListener
	, public Ogre::FrameListener
{
	public:
		TutorialApplication();
		virtual ~TutorialApplication();
 
		bool go();
 
	private:
		virtual bool frameRenderingQueued(const Ogre::FrameEvent& fe);
 
		virtual void windowResized(Ogre::RenderWindow* rw);
		virtual void windowClosed(Ogre::RenderWindow* rw);
 
		// Root object instance for the application
		Ogre::Root* mRoot;

		// Resource configuration file name
		Ogre::String mResourcesCfg;

		// Plugin configuration file name
		Ogre::String mPluginsCfg;

		// Window object instance for rendering the application
		Ogre::RenderWindow* mWindow;

		// Scene manager object that controls all things rendered on the screen
		Ogre::SceneManager* mSceneMgr;

		// Camera object for viewing the scene
		Ogre::Camera* mCamera;
 
		// Declare Object-oriented Input System (OIS) manager
		OIS::InputManager* mInputMgr;

		// Declare keyboard input device object
		OIS::Keyboard* mKeyboard;

		// Declare mouse input device object
		OIS::Mouse* mMouse;
 
};

//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------
