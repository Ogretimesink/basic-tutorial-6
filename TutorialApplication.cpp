/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
	: mRoot(0)
	, mResourcesCfg(Ogre::StringUtil::BLANK)
	, mPluginsCfg(Ogre::StringUtil::BLANK)
	, mWindow(0)
	, mSceneMgr(0)
	, mCamera(0)
	, mInputMgr(0)
	, mMouse(0)
	, mKeyboard(0)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
	// Remove the window as a Window listener
	Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);
 
	// Destroys OIS when the window is closed
	windowClosed(mWindow);
 
	// Destory the application root object instance
	delete mRoot;
}
//---------------------------------------------------------------------------
bool TutorialApplication::go()
{
	#ifdef _DEBUG
		// Set the debug build resource configuration files
		mResourcesCfg = "resources_d.cfg";

		// Set the debug build plugin configuration files
		mPluginsCfg = "plugins_d.cfg";
	#else
		// Set the release build resource configuration files
		mResourcesCfg = "resources.cfg";

		// Set the release build plugin configuration files
		mPluginsCfg = "plugins.cfg";
	#endif

	// Create root object instance for the application
	mRoot = new Ogre::Root(mPluginsCfg);
 
	// Declare object for configuration file
	Ogre::ConfigFile cf;

	// Load settings data from configuration file
	cf.load(mResourcesCfg);
 
	// Declare strings into which to load configuration data
	Ogre::String name, locType;

	// Declare an iterator to load sections of the resource configuration file
	Ogre::ConfigFile::SectionIterator secIt = cf.getSectionIterator();
 
	// Iterate through the resource configuration file
	while (secIt.hasMoreElements())
	{
		// Declare multimap settings pair and load a section
		Ogre::ConfigFile::SettingsMultiMap* settings = secIt.getNext();

		// Declare iterator for multimap items in section
		Ogre::ConfigFile::SettingsMultiMap::iterator it;
 
		// Loop through multimap settings in the section
		for (it = settings->begin(); it != settings->end(); ++it)
		{
			// Unpack the resource location type
			locType = it->first;

			// Unpack the resource location path
			name = it->second;
 
			// Add the unpacked resources to the application 
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(name, locType);
		}
	}
 
	// Load ogre.cfg file, otherwise show the configuration dialog box
	if (!(mRoot->restoreConfig() || mRoot->showConfigDialog()))
		return false;
 
	// Initialize the rendering window using the chosen rendering system
	mWindow = mRoot->initialise(true, "TutorialApplication Render Window");
 
	// Set the default number of texture resolution versions
	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

	// Initialize all of the resources found by the application
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

	// Initialize the scene manager object
	mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC);
 
	// Initialize the scene camera
	mCamera = mSceneMgr->createCamera("MainCam");

	// Position the camera
	mCamera->setPosition(0, 0, 80);

	// Set a position to point the camera
	mCamera->lookAt(0, 0, -300);

	// Set the distance the camera won't render meshes
	mCamera->setNearClipDistance(5);

	// Declare and initialize a scene rendering viewpoint for the camera
	Ogre::Viewport* vp = mWindow->addViewport(mCamera);

	// Set the scene background color
	vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

	// Set the viewport size
	mCamera->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
 
	// Create an instance of a computer graphic model character
	Ogre::Entity* ogreEntity = mSceneMgr->createEntity("ogrehead.mesh");
 
	// Create an object in the scene
	Ogre::SceneNode* ogreNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();

	// Add the entity to the object
	ogreNode->attachObject(ogreEntity);

	// Add light which illuminates all objects in the scene regardless of direction
	mSceneMgr->setAmbientLight(Ogre::ColourValue(.5, .5, .5));
 
	// Create a directional light that gives shadows
	Ogre::Light* light = mSceneMgr->createLight("MainLight");

	// Position the light
	light->setPosition(20, 80, 50);
 
	// Write message to Ogre.log
	Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
 
	// Create a list of parameters
	OIS::ParamList pl;

	// Declare unsigned integer for the window handle
	size_t windowHandle = 0;

	// Declare stream for the name of window handle
	std::ostringstream windowHandleStr;
 
	// Get the window handle
	mWindow->getCustomAttribute("WINDOW", &windowHandle);

	// Bitwise left shift?? 
	windowHandleStr << windowHandle;

	// Pass the name and window handle as a pair to the list
	pl.insert(std::make_pair(std::string("WINDOW"), windowHandleStr.str()));
 
	// Create the input device manager
	mInputMgr = OIS::InputManager::createInputSystem(pl);

	// Create the keyboard input device object with unbuffered input
	mKeyboard = static_cast<OIS::Keyboard*>(mInputMgr->createInputObject(OIS::OISKeyboard, false));

	// Create the mouse input device object with unbuffered input
	mMouse = static_cast<OIS::Mouse*>(mInputMgr->createInputObject(OIS::OISMouse, false));
 
	// Synchronize the OIS mouse state with the actual size of the window
	windowResized(mWindow);

	// Register the window as a Window listener
	Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);
 
	// Add this function as a frame listener
	mRoot->addFrameListener(this);
 
	// Start rendering the scene
	mRoot->startRendering();
 
	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
	// Check the window
	if (mWindow->isClosed())

		// End the application
		return false;
 
	// Check keyboard state
	mKeyboard->capture();

	// Check mouse state
	mMouse->capture();

	// Check the escape key position
	if (mKeyboard->isKeyDown(OIS::KC_ESCAPE))

		// End the application
		return false;
 
	return true;
}
//---------------------------------------------------------------------------
void TutorialApplication::windowResized(Ogre::RenderWindow* rw)
{
	// Declare rendering area variables
	int left, top;

	// Declare rendering area variables
	unsigned int width, height, depth;
 
	// Get information about the rendering area
	rw->getMetrics(width, height, depth, left, top);
 
	// Retrieve the current state of the mouse
	const OIS::MouseState& ms = mMouse->getMouseState();

	// Set the mouse clipping area width to that of the display area
	ms.width = width;

	// Set the mouse clipping area height to that of the display area
	ms.height = height;
}
//---------------------------------------------------------------------------
void TutorialApplication::windowClosed(Ogre::RenderWindow* rw)
{
	// Check for valid application window
	if(rw == mWindow)
	{
		// Check for valid input device manager
		if(mInputMgr)
		{
			// Destroy the mouse input device object
			mInputMgr->destroyInputObject(mMouse);

			// Destroy the keyboard input device object
			mInputMgr->destroyInputObject(mKeyboard);
 
			// Destroy the input device manager
			OIS::InputManager::destroyInputSystem(mInputMgr);

			// Re-initialize the input device manager before application shutdown
			mInputMgr = 0;
		}
	}
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
